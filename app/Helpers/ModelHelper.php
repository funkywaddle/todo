<?php


namespace App\Helpers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Eloquent\Model;

class ModelHelper {
    public static function CleanseData(Model $model, $data) : array {
        $table = $model->getTable();
        $new_data = [];

        foreach($data as $field => $value) {
            if(Schema::hasColumn($table, $field)){
                $new_data[$field] = $value;
            }
        }

        return $new_data;
    }
}
