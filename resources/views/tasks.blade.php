<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Tasks To-Do</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

        <!-- Styles -->
        <link href="/css/styles.css" rel="stylesheet" />
    </head>
    <body>
        <main class="py-4">
            <div class="container-fluid" id="app">
                <div class="row justify-content-center">
                    <div class="col-lg-3 col-sm-12">
                        <div class="card">
                            <div class="card-header">Add New Task</div>
                            <div class="card-body">
                                <div v-if="has_error" class="error" v-html="error"></div>
                                <div class="form-group">
                                    <label for="taskName">Name *</label>
                                    <input type="text" class="form-control" v-model="task.name" placeholder="Enter Task Name" />
                                </div>
                                <div class="form-group">
                                    <label for="taskDesc">Description *</label>
                                    <textarea class="form-control" v-model="task.description" name="taskDesc"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="targetCompletedOn">Target Completion Date *</label>
                                    <input type="date" class="form-control" v-model="task.target_completed_on" name="targetCompletedOn" />
                                </div>
                                <div class="form-group">
                                    <label for="completedOn">Completion Date</label>
                                    <input type="date" class="form-control" v-model="task.completed_on" name="completedOn" />
                                </div>
                            </div>
                            <div class="card-footer">
                                <button @click="createTask" class="btn btn-primary">Create Task</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-9 col-sm-12">
                        <div class="tasks" id="taskTable">
                            <table class="table table-striped">
                                <thead class="thead-dark">
                                <tr>
                                    <th scope="col">Completed</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Description</th>
                                    <th scope="col">Target Completion</th>
                                    <th scope="col">Completed On</th>
                                    <th scope="col">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <tr is="task-item" v-bind:key="task.id" v-bind:task="task" v-for="task in tasks" @deletetask="deletetask"></tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <!-- Bootstrap -->
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
        <!-- Vue -->
        <script src="https://cdn.jsdelivr.net/npm/vue"></script>
        <!-- Axios -->
        <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
        <script src="js/site.js" type="text/javascript"></script>
    </body>
</html>
