<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Interfaces\Service;

class TaskController extends Controller
{
    private $service;

    public function __construct(Service $service) {
        $this->service = $service;
    }

    public function index() {
        return $this->service->getList();
    }

    public function store(Request $request) {
        $data = $request->validateWithBag('task', $this->getValidation());
        return $this->service->addSingle($data);
    }

    public function show($id) {
        return $this->service->getSingle($id);
    }

    public function update(Request $request, $id) {
        $data = $request->validateWithBag('task', $this->getValidation());
        return $this->service->updateSingle($id, $data);
    }

    public function destroy($id) {
        return $this->service->deleteSingle($id);
    }

    private function getValidation() {
        return [
            'name' => ['required', 'max:255'],
            'description' => ['required'],
            'target_completed_on' => ['required','date'],
            'completed_on' => ['nullable','date'],
            'completed' => ['nullable', 'boolean']
        ];
    }
}
