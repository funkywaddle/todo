<?php

namespace App\Interfaces;

interface Service {

    public function getList();
    public function getSingle($id);
    public function addSingle($data);
    public function updateSingle($id, $data);
    public function deleteSingle($id);

}
