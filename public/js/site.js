function parseErrors(error) {
    let message = error.response.data.message + '<ul>';
    for (let field in error.response.data.errors) {
        message += "<li>" + error.response.data.errors[field] + "</li>";
    }
    message += "</ul>";
    return message;
}

function addItem(task) {
    return axios.post('/api/tasks', task);
}

function updateItem(task) {
    return axios.put('/api/tasks/' + task.id, task)
}

function deleteItem(task) {
    return axios.delete('/api/tasks/' + task.id);
}

Vue.component('edit-button', {
    props: ['task'],
    data: function () {
        return {
            editTask: Object.assign({}, this.task),
            has_error: false,
            error: null
        }
    },
    methods: {
        updateItem: function () {
            let self = this;
            updateItem(self.editTask)
            .then(function (response) {
                self.has_error = false;
                self.error = "";
                Object.assign(self.task, self.editTask);
                $(self.targetId(true)).modal('hide');
            })
            .catch(function(error) {
                self.has_error = true;
                self.error = parseErrors(error);
            });
        },
        targetId: function (addHash) {
            let id = 'edit' + this.task.id;
            return addHash ? '#' + id : id;
        }
    },
    template: `
    <span>
        <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" :data-target="targetId(true)">
            Edit
        </button>
        <div class="modal fade" :id="targetId(false)" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit {{editTask.name}}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div v-if="has_error" class="error" v-html="error"></div>
                        <div class="form-group">
                            <label for="taskName">Name *</label>
                            <input type="text" class="form-control" v-model="editTask.name" />
                        </div>
                        <div class="form-group">
                            <label for="taskDesc">Description *</label>
                            <textarea class="form-control" v-model="editTask.description"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="targetCompletedOn">Target Completion Date *</label>
                            <input type="date" class="form-control" v-model="editTask.target_completed_on" />
                        </div>
                        <div class="form-group">
                            <label for="completedOn">Completion Date</label>
                            <input type="date" class="form-control" v-model="editTask.completed_on" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" @click="updateItem">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    </span>
    `
});

Vue.component('delete-button', {
    props: ['task'],
    data: function () {
        return {
            has_error: false,
            error: null
        }
    },
    methods: {
        targetId: function (addHash) {
            let id = 'delete' + this.task.id;
            return addHash ? '#' + id : id;
        },
        deleteItem: function () {
            let self = this;
            deleteItem(this.task)
            .then(function(response){
                self.has_error = false;
                self.error = "";
                $(self.targetId(true)).modal('hide');
                self.$emit('deletetask', self.task);
            })
            .catch(function(error){
                self.has_error = true;
                self.error = parseErrors(error);
            });
        }
    },
    template: `
    <span>
        <button type="button" class="btn btn-sm btn-danger" data-toggle="modal" :data-target="targetId(true)">
            Delete
        </button>
        <div class="modal fade" :id="targetId(false)" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit {{task.name}}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div v-if="has_error" class="error" v-html="error"></div>
                        <p>Are you sure you wish to delete Task: {{task.name}}?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-danger" @click="deleteItem">Delete Task</button>
                    </div>
                </div>
            </div>
        </div>
    </span>
    `
});

Vue.component('task-item', {
    props: ['task'],
    computed: {
        completedclass: function () {
            return this.task.completed ? 'completed' : '';
        }
    },
    watch: {
        completedclass: function (newValue) {
            if (newValue === 'completed') {
                if (this.task.completed_on === '' || this.task.completed_on === null) {
                    let today = new Date();
                    let dd = today.getDate();
                    let mm = today.getMonth() + 1;
                    let yyyy = today.getFullYear();
                    dd = (dd < 10) ? '0' + dd : dd;
                    mm = (mm < 10) ? '0' + mm : mm;
                    this.task.completed_on = yyyy + '-' + mm + '-' + dd;
                }
            } else {
                this.task.completed_on = '';
            }
            this.updateItem();
        },
        task: {
            immediate: true,
            deep: true,
            handler(newValue, oldValue) {
                this.task.completed = !(this.task.completed_on === '' || this.task.completed_on === null);
            }
        }
    },
    methods: {
        updateItem: function () {
            let self = this;
            updateItem(this.task)
            .then(function(response){
                self.has_error = false;
                self.error = "";
            })
            .catch(function(error){
                self.has_error = true;
                self.error = parseErrors(error);
            });
        },
        deleteItemHandler: function (task) {
            this.$emit('deletetask', task);
        }
    },
    template: `
        <tr :id="task.id" :class="completedclass">
            <td>
                <input type="checkbox" v-model="task.completed">
            </td>
            <td>{{ task.name }}</td>
            <td>{{ task.description }}</td>
            <td>{{ task.target_completed_on }}</td>
            <td>{{ task.completed_on }}</td>
            <td>
                <edit-button :task="task"></edit-button>
                <delete-button :task="task" @deletetask="deleteItemHandler"></delete-button>
            </td>
        </tr>
    `
});

new Vue({
    el: '#app',
    data: {
        task: {
            name: null,
            description: null,
            target_completed_on: null,
            completed_on: null,
            completed: false
        },
        tasks: null,
        error: "",
        has_error: true
    },
    methods: {
        createTask: function () {
            let self = this;
            addItem(this.task)
                .then(function (response) {
                    self.tasks.push(response.data);
                    self.clearForm();
                })
                .catch(function (error) {
                    self.has_error = true;
                    self.error = parseErrors(error);
                })
        },
        clearForm: function() {
            this.has_error = false;
            this.error = "";
            this.task = {
                name: null,
                description: null,
                target_completed_on: null,
                completed_on: null,
                completed: false
            };
        },
        deletetask: function (task) {
            this.tasks.splice(this.tasks.indexOf(task), 1);
        }
    },
    mounted() {
        axios
            .get('/api/tasks')
            .then(response => (this.tasks = response.data))
    }
});
