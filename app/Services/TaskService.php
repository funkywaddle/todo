<?php
namespace App\Services;

use App\Interfaces\Service;
use App\Models\Task;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use App\Helpers\ModelHelper;

class TaskService implements Service {

    private $task;

    public function __construct(Model $task) {
        $this->task = $task;
    }

    public function getList() : Collection {
        return $this->task->get();
    }

    public function getSingle($id) : Task {
        return $this->task->where('id', $id)->first();
    }

    public function addSingle($data) : Task {
        $data = ModelHelper::CleanseData($this->task, $data);
        $task = $this->task->newInstance($data);
        $task->save();
        return $task;
    }

    public function updateSingle($id, $data) : void {
        $data = ModelHelper::CleanseData($this->task, $data);
        $task = $this->getSingle($id);
        $task->fill($data);
        $task->save();
    }

    public function deleteSingle($id) : void {
        $this->task->destroy($id);
    }
}
