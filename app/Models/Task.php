<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Task extends Model {
    protected $table = 'todos';
    public $timestamps = false;
    protected $fillable = ['name', 'description', 'target_completed_on', 'completed_on', 'completed'];
    protected $appends = ['completed'];

    public function getCompletedAttribute() {
        return !empty($this->completed_on);
    }
}
