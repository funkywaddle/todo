<?php

namespace App\ServiceProviders;

use App\Models\Task;
use App\Services\TaskService;
use Illuminate\Support\ServiceProvider;

class TaskServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(TaskService::class, function ($app) {
            return new TaskService(new Task());
        });

        $this->app->when('App\Http\Controllers\TaskController')
            ->needs('App\Interfaces\Service')
            ->give(TaskService::class);
    }
}
